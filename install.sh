#!/bin/bash

sudo apt-get install -y python3-dev
sudo apt-get install -y python3-pip
pip3 install wheel
pip3 install -r requirements.txt
