#!python

import os
import sys
import struct
from typing.io import BinaryIO

from elftools.elf.elffile import ELFFile
from elftools.elf.relocation import RelocationSection
from elftools.elf.sections import SymbolTableSection


class MorgothError(Exception):
    pass

SHF_WRITE = 0x01
SHF_ALLOC = 0x02
SHF_EXECINSTR = 0x04

PT_LOAD = 0x01

P_TYPE_OFFSET = 0x0
P_FLAGS_OFFSET = 0x4
P_OFFSET_OFFSET = 0x8
P_VADDR_OFFSET = 0x10
P_PADDR_OFFSET = 0x18
P_FILESZ_OFFSET = 0x20
P_MEMSZ_OFFSET = 0x28
P_ALIGN_OFFSET = 0x30

ELFHEADER_SIZE = 0x40
E_SHOFF_OFFSET = 0x28
E_PHNUM_OFFSET = 0x38

SH_OFFSET_OFFSET = 0x18

GAP_SIZE = 0x1000
DEFAULT_SEGMENT_ALIGN = 0x1000

R_X86_64_64 = 1
R_X86_64_PC32 = 2
R_X86_64_PLT32 = 4
R_X86_64_32 = 10
R_X86_64_32S = 11


def print_hello():
    print("""
Melkor  (Quenya; IPA: "He who arises in might"), later known predominantly as Morgoth 
(Sindarin;  IPA:  "Black  Foe  of  the  World"), was  the  first  Dark  Lord, and the 
primordial  source  of evil in Eä. Melkor discovered the Elves before the other Valar, 
captured many of them, and transformed them by torture and other foul craft into orcs.""")


def write_u32(file, position, value):
    file.seek(position)
    file.write(struct.pack('<L', value))


def write_u64(file, position, value):
    file.seek(position)
    file.write(struct.pack('<Q', value))


def do_align(value, align):
    if value % align != 0:
        value = ((value // align) + 1) * align
    return value


def load_payload_elf(elf_file):
    payload_elf = ELFFile(elf_file)
    if payload_elf.header['e_type'] != 'ET_REL':
        raise MorgothError(f'Invalid payload e_type: {payload_elf.header["e_type"]}, expected ET_REL')
    print('==================PAYLOAD  ELF==================')
    print(payload_elf.header)
    return payload_elf


def load_target_elf(elf_file):
    target_elf = ELFFile(elf_file)
    if target_elf.header['e_type'] != 'ET_EXEC':
        raise MorgothError(f'Invalid target e_type: {target_elf.header["e_type"]}, expected ET_EXEC')
    if target_elf.header['e_phoff'] != target_elf.header['e_ehsize']:
        raise MorgothError(f'Elf file header size does not equal program header offset')
    print('===================TARGET ELF===================')
    print(target_elf.header)
    return target_elf


def write_new_segment_header(elf: BinaryIO, base_addr: int, flags: int, offset: int, addr: int, size: int):
    write_u32(elf, base_addr + P_TYPE_OFFSET, PT_LOAD)   # Elf64_Word p_type; u32
    write_u32(elf, base_addr + P_FLAGS_OFFSET, flags)    # Elf64_Word p_flags; u32
    write_u64(elf, base_addr + P_OFFSET_OFFSET, offset)  # Elf64_Off p_offset; u64
    write_u64(elf, base_addr + P_VADDR_OFFSET, addr)     # Elf64_Addr p_vaddr; u64
    write_u64(elf, base_addr + P_PADDR_OFFSET, addr)     # Elf64_Addr p_paddr; u64
    write_u64(elf, base_addr + P_FILESZ_OFFSET, size)    # Elf64_Xword p_filesz; u64
    write_u64(elf, base_addr + P_MEMSZ_OFFSET, size)     # Elf64_Xword p_memsz; u64
    write_u64(elf, base_addr + P_ALIGN_OFFSET, DEFAULT_SEGMENT_ALIGN)    # Elf64_Xword p_align; u64


def create_copy_with_gap(target_elf, target_elf_file, new_elf_file):
    print("Creating a copy of target elf...")
    # elf header and program header
    target_elf_file.seek(0)
    elf_header_bytes = target_elf_file.read(ELFHEADER_SIZE)
    new_elf_file.write(elf_header_bytes)
    program_header_bytes = target_elf_file.read(target_elf.header['e_phnum'] * target_elf.header['e_phentsize'])
    new_elf_file.write(program_header_bytes)
    # empty page
    new_elf_file.write(bytes([0x0] * GAP_SIZE))
    # rest of the file
    rest_of_the_target_elf = target_elf_file.read()
    new_elf_file.write(rest_of_the_target_elf)


def update_section_header_offset(target_elf, new_elf_file):
    print("Updating section header offset...")
    new_section_headers = target_elf.header['e_shoff'] + GAP_SIZE
    write_u64(new_elf_file, E_SHOFF_OFFSET, new_section_headers)
    return new_section_headers


def update_section_headers(target_elf, new_elf_file, new_section_headers):
    print("Updating section headers...")
    for i, section in enumerate(target_elf.iter_sections()):
        new_section_location = section['sh_offset'] + GAP_SIZE
        write_u64(new_elf_file,
                  new_section_headers + target_elf.header['e_shentsize'] * i + SH_OFFSET_OFFSET,
                  new_section_location)


def update_program_headers(target_elf, new_elf_file):
    print("Updating program headers...")
    end_of_memory = 0
    for i, segment in enumerate(target_elf.iter_segments()):
        end_of_memory = max(segment['p_vaddr'] + segment['p_memsz'], end_of_memory)
        ith_header_offset = ELFHEADER_SIZE + target_elf.header['e_phentsize'] * i
        if (segment['p_type']) == 'PT_PHDR':
            # memsz/filesz of this segment is modified later to make space for new program headers
            write_u64(new_elf_file, ith_header_offset + P_VADDR_OFFSET, segment['p_vaddr'] - GAP_SIZE)
            write_u64(new_elf_file, ith_header_offset + P_PADDR_OFFSET, segment['p_paddr'] - GAP_SIZE)
        elif segment['p_type'] == 'PT_LOAD' and segment['p_offset'] == 0x0:
            # modify first load to account for the gap
            write_u64(new_elf_file, ith_header_offset + P_VADDR_OFFSET, segment['p_vaddr'] - GAP_SIZE)
            write_u64(new_elf_file, ith_header_offset + P_PADDR_OFFSET, segment['p_paddr'] - GAP_SIZE)
            write_u64(new_elf_file, ith_header_offset + P_FILESZ_OFFSET, segment['p_filesz'] + GAP_SIZE)
            write_u64(new_elf_file, ith_header_offset + P_MEMSZ_OFFSET, segment['p_memsz'] + GAP_SIZE)
        elif segment['p_type'] != 'PT_GNU_STACK':
            write_u64(new_elf_file, ith_header_offset + P_OFFSET_OFFSET, segment['p_offset'] + GAP_SIZE)
    print(f'    end_of_memory: {hex(end_of_memory)}')
    return end_of_memory


def gather_sections_to_move(payload_elf):
    sections_by_flags = {}
    for i, section in enumerate(payload_elf.iter_sections()):
        if section['sh_flags'] & SHF_ALLOC:  # SHF_ALLOC
            # Convert section flags (W_X) to segment flags (XWR)
            flags = ((section['sh_flags'] & SHF_WRITE) << 1) + ((section['sh_flags'] & SHF_EXECINSTR) >> 2) + 4
            if flags not in sections_by_flags:
                sections_by_flags[flags] = []
            sections_by_flags[flags].append(section)
    return sections_by_flags


def create_new_sections(target_elf, payload_elf_file, new_elf_file, new_elf_size, end_of_memory, sections_by_flags):
    cursor = new_elf_size
    segment_begin = cursor
    memory_segment_begin = end_of_memory
    headers_n = target_elf.header['e_phnum']
    payload_section_memory_placements = {}
    payload_section_file_placements = {}
    for flags, section_list in sections_by_flags.items():
        print(f'Browsing segment for flags: {flags}, sections: [{[s.name for s in section_list]}]')
        cursor = do_align(cursor, DEFAULT_SEGMENT_ALIGN)
        memory_segment_begin = do_align(memory_segment_begin, DEFAULT_SEGMENT_ALIGN)
        segment_nonempty = False
        for i, section in enumerate(section_list):
            if section['sh_size'] > 0:
                segment_nonempty = True
                cursor = do_align(cursor, section['sh_addralign'])
                if i == 0:
                    segment_begin = cursor
                payload_section_memory_placements[section.name] = memory_segment_begin + (cursor - segment_begin)
                payload_section_file_placements[section.name] = cursor
                print(f'    Section {section.name} (type: {section["sh_type"]}, size: {section["sh_size"]}) will reside at file: {hex(payload_section_file_placements[section.name])}, memory: {hex(payload_section_memory_placements[section.name])}')
                if section['sh_type'] == 'SHT_NOBITS':
                    new_elf_file.seek(cursor)
                    new_elf_file.write(bytes([0x0] * section['sh_size']))
                else:
                    payload_elf_file.seek(section['sh_offset'])
                    section_bytes = payload_elf_file.read(section['sh_size'])
                    new_elf_file.seek(cursor)
                    new_elf_file.write(section_bytes)
                cursor += section['sh_size']
            else:
                print(f'    Section {section.name} is empty, ignoring.')
        if segment_nonempty:
            print(f'Writing segment for flags: {flags}, sections: [{[s.name for s in section_list]}]')
            header_addr = 0x40 + target_elf.header['e_phentsize'] * headers_n
            headers_n += 1
            write_new_segment_header(new_elf_file, header_addr, flags, segment_begin, memory_segment_begin,
                                     cursor - segment_begin)
            memory_segment_begin += cursor - segment_begin

    # Update number of program headers
    new_elf_file.seek(E_PHNUM_OFFSET)
    new_elf_file.write(struct.pack('<H', headers_n))  # u16

    # Update the size of program header segment
    for i, segment in enumerate(target_elf.iter_segments()):
        if (segment['p_type']) == 'PT_PHDR':
            write_u64(new_elf_file,
                      0x40 + target_elf.header['e_phentsize'] * i + P_FILESZ_OFFSET,
                      target_elf.header['e_phentsize'] * headers_n)
            write_u64(new_elf_file,
                      0x40 + target_elf.header['e_phentsize'] * i + P_MEMSZ_OFFSET,
                      target_elf.header['e_phentsize'] * headers_n)

    return payload_section_memory_placements, payload_section_file_placements


def perform_relocations(payload_elf, target_elf, new_elf_file, payload_section_file_placements,
                        payload_section_memory_placements, t_symbol_section, p_symbol_section):
    for p_section in payload_elf.iter_sections():
        if isinstance(p_section, RelocationSection):
            print(f'Found RelocationSection: {p_section.name}, n: {p_section.num_relocations()}')
            p_target_section = payload_elf.get_section_by_name(p_section.name[5:])  # .rela.####
            for reloc in p_section.iter_relocations():
                if not reloc.is_RELA():
                    raise MorgothError('Invalid relocation encountered (not RELA)')

                p_symbol_index = reloc["r_info"] >> 32
                p_type = struct.unpack_from("I", struct.pack("q", reloc["r_info"]))[0]

                if p_type not in [1, 10, 11, 2, 4]:
                    print(f'    WARN: Unrecognized relocation type: {p_type}, ignoring')
                    continue

                if p_type == R_X86_64_64:       # S + A     / qword
                    relocation_type = 'R_X86_64_64'
                elif p_type == R_X86_64_PC32:   # S + A - P / dword
                    relocation_type = 'R_X86_64_PC32'
                elif p_type == R_X86_64_PLT32:  # L + A - P / dword
                    relocation_type = 'R_X86_64_PLT32'
                elif p_type == R_X86_64_32:     # S + A     / dword
                    relocation_type = 'R_X86_64_32'
                elif p_type == R_X86_64_32S:    # S + A     / dword
                    relocation_type = 'R_X86_64_32S'

                p_symbol = p_symbol_section.get_symbol(p_symbol_index)
                if p_symbol.entry["st_shndx"] != 'SHN_UNDEF':
                    p_symbol_target_section = payload_elf.get_section(p_symbol.entry["st_shndx"])
                else:
                    p_symbol_target_section = None
                p_symbol_target_section_name = p_symbol_target_section.name if p_symbol_target_section is not None else 'SHN_UNDEF'
                p_symbol_st_info_type = p_symbol.entry["st_info"]["type"]

                if p_symbol_st_info_type not in ['STT_NOTYPE', 'STT_FUNC', 'STT_OBJECT', 'STT_SECTION']:
                    print(f'    WARN: Unrecognized relocation symbol type: {p_symbol_st_info_type}, ignoring')
                    continue

                print(f'''    {relocation_type} ::: r_offset: {reloc["r_offset"]}, r_addend: {reloc["r_addend"]}, \
symbol[name: {p_symbol.name}, type: {p_symbol_st_info_type}, section: {p_symbol_target_section_name}, value: {p_symbol.entry["st_value"]}]''')

                target_section_addr = payload_section_file_placements[p_target_section.name]
                target_addr = target_section_addr + reloc["r_offset"]
                memory_target_addr = payload_section_memory_placements[p_target_section.name] + reloc["r_offset"]

                if p_symbol_target_section is not None:
                    section_containing_symbol_addr = payload_section_memory_placements[
                        p_symbol_target_section.name]
                    symbol_addr = section_containing_symbol_addr + p_symbol["st_value"]
                else:
                    if p_symbol.name != 'orig_start':
                        t_symbol = t_symbol_section.get_symbol_by_name(p_symbol.name)

                        if t_symbol is None:
                            raise MorgothError(f'Invalid relocation, symbol {p_symbol.name} not found in target.')

                        t_symbol = t_symbol[0]  # What? Multiple symbols? I can't hear you ~ la la la
                        if t_symbol.entry["st_shndx"] == 'SHN_UNDEF':
                            raise MorgothError(f'Invalid relocation, symbol {p_symbol.name} has section SHN_UNDEF')

                        symbol_addr = t_symbol["st_value"]
                    else:
                        symbol_addr = target_elf.header["e_entry"]

                if p_type in [2, 4]:
                    print(f'        PC-relative: yes, ({hex(symbol_addr)}) + {hex(reloc["r_addend"])} - {hex(memory_target_addr)}')
                    addr = symbol_addr + reloc["r_addend"] - memory_target_addr
                else:
                    print(f'        PC-relative: no, ({hex(symbol_addr)}) + {hex(reloc["r_addend"])}')
                    addr = symbol_addr + reloc["r_addend"]

                if p_type == 1:
                    addr_bytes = struct.pack('<q', addr)
                else:
                    addr_bytes = struct.pack('<l', addr)

                print(f'        Replacing space at {hex(target_addr)} with address {hex(addr)}')
                new_elf_file.seek(target_addr)
                new_elf_file.write(addr_bytes)


def update_entry_address(new_elf_file, p_symbol_section, payload_section_memory_placements):
    start = p_symbol_section.get_symbol_by_name('_start')
    if start is None:
        print('WARN: No _start symbol in payload')
        return
    start = start[0]
    start_addr = payload_section_memory_placements['.text'] + start.entry['st_value']
    new_elf_file.seek(0x18)
    new_elf_file.write(struct.pack('<Q', start_addr))


def process_file(target_elf_path, payload_elf_path, new_elf_path):
    print_hello()
    print('================================================')
    print(f'Postlinking {payload_elf_path} to {target_elf_path}')
    print(f'Creating {new_elf_path}')

    with open(payload_elf_path, 'rb') as payload_elf_file, \
            open(new_elf_path, 'wb') as new_elf_file, \
            open(target_elf_path, 'rb') as target_elf_file:

        try:
            payload_elf = load_payload_elf(payload_elf_file)
            target_elf = load_target_elf(target_elf_file)
            print('================================================')

            t_symbol_section = target_elf.get_section_by_name('.symtab')
            if isinstance(t_symbol_section, SymbolTableSection):
                print(f'Found SymbolTableSection in target: {t_symbol_section.name}')
            else:
                raise MorgothError('Did not find symbol section in target')

            p_symbol_section = payload_elf.get_section_by_name('.symtab')
            if isinstance(p_symbol_section, SymbolTableSection):
                print(f'Found SymbolTableSection in payload: {p_symbol_section.name}')
            else:
                raise MorgothError('Did not find symbol section in payload')

            print('================================================')
            create_copy_with_gap(target_elf, target_elf_file, new_elf_file)
            new_section_headers = update_section_header_offset(target_elf, new_elf_file)
            update_section_headers(target_elf, new_elf_file, new_section_headers)
            end_of_memory = update_program_headers(target_elf, new_elf_file)

            print('===============SECTIONS BY FLAGS===============')
            sections_by_flags = gather_sections_to_move(payload_elf)

            new_elf_size = os.path.getsize(target_elf_path) + 0x1000
            payload_section_memory_placements, payload_section_file_placements = create_new_sections(target_elf,
                payload_elf_file, new_elf_file, new_elf_size, end_of_memory, sections_by_flags)

            print('==================RELOCATIONS ==================')
            perform_relocations(payload_elf, target_elf, new_elf_file, payload_section_file_placements,
                                payload_section_memory_placements, t_symbol_section, p_symbol_section)

            update_entry_address(new_elf_file, p_symbol_section, payload_section_memory_placements)

        except MorgothError as e:
            print('=================ERROR OCCURRED=================')
            print(e)
            print(f'Args: {e.args}')


if __name__ == '__main__':
    if len(sys.argv) == 4:
        process_file(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("Invalid number of arguments, expected 3")
